#How to run project

<p>project root here ~/dev/mrslotty_test_task_2/ change your project root accordingly </p

## Create users 
```bash
sudo useradd ws_checker --system --gid webapps --shell /bin/bash -d ~/dev/mrslotty_test_task_2/
sudo passwd <your_pwd_here>
sudo groupadd webapps
sudo chgrp -R webapps /var/www/
sudo chmod -R 760 /var/www/
```
## Change code access (idk)
```bash
sudo su - ws_checker
sudo chown -R ws_checker:webapps ~/dev/mrslotty_test_task_2
sudo chmod -R g+w ~/dev/mrslotty_test_task_2
```

## Install fresh UWSGI (xenial packets are outdated)

```bash
git clone https://github.com/unbit/uwsgi/releases/tag/2.0.17.1
tar zxvf uwsgi-2.0.17.1.tar.gz

```
## Mount front-end folders
```bash
ls -d /home/usr/dev/mrslotty_test_task_2/ws_checker.gunicorn.sock
sudo mount --bind ~/dev/mrslotty_test_task_2/build/ ~/dev/mrslotty_test_task_2/ws_checker/templates
sudo mount --bind ~/dev/mrslotty_test_task_2/build/ /home/usr/dev/mrslotty_test_task_2/ws_checker/frontend/static
sudo ln -S ~/dev/mrslotty_test_task_2/ws_checker/templates/index.demo.html ~/dev/mrslotty_test_task_2/public/index.html
mkdir -P /home/usr/dev/mrslotty_test_task_2/ws_checker/templates/main/
touch /home/usr/dev/mrslotty_test_task_2/ws_checker/templates/main/index.html
sudo mount --bind  ~/dev/mrslotty_test_task_2/build/index.html /home/usr/dev/mrslotty_test_task_2/ws_checker/templates/main/index.html
```
