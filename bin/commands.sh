#!/usr/bin/env bash


sudo cp -R ~/dev/mrslotty_test_task/init/etc/* /etc/
sudo ln -s /etc/nginx/sites-available/ws_checker /etc/nginx/sites-enabled/ws_checker


sudo cp -R ~/dev/mrslotty_test_task/init/etc/* /etc/
sudo systemctl daemon-reload
sudo systemctl restart nginx.service
journalctl -xe

sudo cp -R ~/dev/mrslotty_test_task/init/etc/* /etc/
sudo systemctl daemon-reload
sudo systemctl start gunicorn.socket
journalctl -xe

sudo cp -R ~/dev/mrslotty_test_task/init/etc/* /etc/
sudo systemctl daemon-reload
sudo systemctl start gunicorn_django_react.service
journalctl -xe


