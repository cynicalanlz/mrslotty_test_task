#!/bin/bash

BASE_DIR="/home/usr/dev"

NAME="ws_checker"                              #Name of the application (*)
PROJECT_NAME="mrslotty_test_task"
DJANGODIR="$BASE_DIR/$PROJECT_NAME"             # Django project directory (*)
SOCKFILE="$BASE_DIR/$PROJECT_NAME/ws_checker.gunicorn.sock"       # we will communicate using this unix socket (*)
USER=ws_checker                                        # the user to run as (*)
GROUP=webdata                                     # the group to run as (*)
NUM_WORKERS=1                                     # how many worker processes should Gunicorn spawn (*)
DJANGO_SETTINGS_MODULE=ws_checker.settings             # which settings file should Django use (*)
DJANGO_WSGI_MODULE=ws_checker.wsgi                     # WSGI module name (*)

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source "$BASE_DIR/$PROJECT_NAME/venv/bin/activate"
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn

gunicorn_path="$BASE_DIR/$PROJECT_NAME/venv/bin/gunicorn"
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec ${gunicorn_path} ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user $USER \
  --bind=unix:$SOCKFILE \
  --chdir=$RUNDIR