#!/usr/bin/env bash

base_path="/home/usr/dev/mrslotty_test_task"
base_front_path="/home/usr/dev/mrslotty_test_task_frontend"
build_front_path=${base_front_path}/build
build_templates=${base_path}/ws_checker/frontend/templates

sudo mount --bind $build_front_path $build_templates
sudo mount --bind $build_front_path ${base_path}/ws_checker/static
sudo mount --bind ${build_front_path}/index.html ${base_path}/ws_checker/templates/main/index.html