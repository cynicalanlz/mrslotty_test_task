#!/usr/bin/env bash

# users
sudo chmod -R o+x /project_folder folders up to static
sudo mkdir /var/www/ws_checker
sudo groupadd webapps
sudo useradd ws_checker --system --gid webapps --shell /bin/bash
sudo chgrp -R webapps /var/www/mrslotty
sudo chmod -R 2775 /var/www/mrslotty
#sudo su - ws_checker
#sudo chown -R ws_checkker:users ~
#sudo chmod -R g+w ~/dev/mrslotty_test_task

# virtualenv

pip install virtualenv
virtualenv --python=/usr/bin/python2.7 venv
source /venv/bin/activate


# uwsgi

#sudo apt-get install uwsgi-plugin-python
#git clone https://github.com/unbit/uwsgi/releases/tag/2.0.17.1
#tar filename
#cd directory
#make
#cp uwsgi /usr/bin /usr/share/bin

# install nginx

sudo wget https://nginx.org/keys/nginx_signing.key
sudo apt-key add nginx_signing.key
sudo apt-get purge nginx nginx-common nginx-full
sudo rm -rf nginx.conf sites-available/default
sudo nano /etc/apt/sources.list
#add there
deb https://nginx.org/packages/mainline/debian/ <CODENAME> nginx
deb-src https://nginx.org/packages/mainline/debian/ <CODENAME> nginx
sudo apt install nginx nginx-common
sudo touch /etc/nginx/nginx.conf

# front-end

sudo npm run build
mkdir /home/usr/dev/mrslotty_test_task/ws_checker/templates/main/
touch /home/usr/dev/mrslotty_test_task/ws_checker/templates/main/index.html