#!/bin/bash

function manage_app () {
    ./manage.py makemigrations
    ./manage.py migrate
}

function start_development() {
    # use django runserver as development server here.
    python manage.py runserver 127.0.0.1:8000
}

function start_production() {
    # use gunicorn for production server here
    systemctl start nginx
    systemctl enable gunicorn.socket

}

if [ ${PRODUCTION} == "false" ]; then
    # use development server
    start_development
else
    # use production server
    start_production
fi