# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models.fields import TextField, IntegerField, BooleanField


class WebsiteUrl(models.Model):
    url = TextField(unique=True)
    freq = IntegerField(default=1) # number of minutes between website refreshes
    is_checked = BooleanField(default=True)

    def save(self, *args, **kwargs):
        self.url = self.url.lower()
        if not self.url.startswith("http"):
            self.url = "http://" + self.url
        return super(WebsiteUrl, self).save(*args, **kwargs)
