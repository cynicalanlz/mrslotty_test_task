# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import WebsiteUrl
from .serializers import WebsiteUrlSerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated


class WebsiteUrlViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = WebsiteUrl.objects.all()
    serializer_class = WebsiteUrlSerializer
