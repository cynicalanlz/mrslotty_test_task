# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from ws.models import WebsiteUrl


class WebsiteUrlCheck(models.Model):
    website_url = models.ForeignKey(WebsiteUrl, related_name='website_urls')
    response_status = models.IntegerField()
    #
    # @property
    # def website_url_text(self):
    #     return self.website_url.url