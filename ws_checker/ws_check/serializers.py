from .models import WebsiteUrl
from rest_framework import serializers


class WebsiteUrlSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WebsiteUrl
        fields = ('url', 'freq', 'is_checked')