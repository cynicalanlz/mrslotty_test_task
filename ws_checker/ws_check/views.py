# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import WebsiteUrlCheck
from .serializers import WebsiteUrlCheckSerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated


class WebsiteUrlCheckViewSet(viewsets.ModelViewSet):
    permission_classes = [ IsAuthenticated ]
    queryset = WebsiteUrlCheck.objects.all()
    serializer_class = WebsiteUrlCheckSerializer